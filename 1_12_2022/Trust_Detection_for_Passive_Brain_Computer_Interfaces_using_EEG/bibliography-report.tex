%% ISAE-SUPAERO report template for research projects 
%% V1.0
%% 2016/04/14
%% by Damien Roque
%% See http://personnel.isae.fr/damien-roque


%% This template is based on bare_conf.tex
%% V1.4b
%% 2015/08/26
%% by Michael Shell

%%*************************************************************************
%% Legal Notice:
%% This code is offered as-is without any warranty either expressed or
%% implied; without even the implied warranty of MERCHANTABILITY or
%% FITNESS FOR A PARTICULAR PURPOSE! 
%% User assumes all risk.
%% In no event shall the IEEE or any contributor to this code be liable for
%% any damages or losses, including, but not limited to, incidental,
%% consequential, or any other damages, resulting from the use or misuse
%% of any information contained here.
%%
%% All comments are the opinions of their respective authors and are not
%% necessarily endorsed by the IEEE.
%%
%% This work is distributed under the LaTeX Project Public License (LPPL)
%% ( http://www.latex-project.org/ ) version 1.3, and may be freely used,
%% distributed and modified. A copy of the LPPL, version 1.3, is included
%% in the base LaTeX documentation of all distributions of LaTeX released
%% 2003/12/01 or later.
%% Retain all contribution notices and credits.
%% ** Modified files should be clearly indicated as such, including  **
%% ** renaming them and changing author support contact information. **
%%*************************************************************************

\documentclass[conference]{IEEEtran}

\usepackage[utf8]{inputenc}
\usepackage{ifthen}
\usepackage{cite}
\usepackage[pdftex]{graphicx}
\graphicspath{{images/}}
\usepackage{tikz,filecontents}
\usetikzlibrary{shapes,arrows,shadings,patterns}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\pgfplotsset{plot coordinates/math parser=false}
\newlength\figureheight
\newlength\figurewidth

\usepackage{amsfonts}
\usepackage[cmex10]{amsmath}
\usepackage{multirow}

% Examples of several macros
\newcommand*{\SET}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand*{\VEC}[1]{\ensuremath{\boldsymbol{\mathrm{#1}}}}
\newcommand*{\FAM}[1]{\ensuremath{\mathrm{#1}}}
\newcommand*{\MAT}[1]{\ensuremath{\boldsymbol{\mathrm{#1}}}}
\newcommand*{\OP}[1]{\ensuremath{\mathrm{#1}}}
\newcommand*{\NORM}[1]{\ensuremath{\left\|#1\right\|}}
\newcommand*{\DPR}[2]{\ensuremath{\left \langle #1,#2 \right \rangle}}

\newtheorem{theorem}{Theorem}

\newcommand{\alert}[1]{\textcolor{red}{#1}}
\usepackage[caption=false,font=footnotesize]{subfig}
\usepackage{url}
\usepackage{hyperref}


% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Trust Detection for Passive Brain Computer Interfaces using EEG}

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
\author{\IEEEauthorblockN{Florian Metzger\IEEEauthorrefmark{1},
Nicolas Drougard\IEEEauthorrefmark{2}\IEEEauthorrefmark{3} and 
Bertille Somon\IEEEauthorrefmark{3}}
\IEEEauthorblockA{\IEEEauthorrefmark{1}Institut Supérieur de l'Aéronautique et de l'Espace (ISAE-SUPAERO), Université de Toulouse, 31055 Toulouse, FRANCE\\
Email: florian.metzger@student.isae-supaero.fr}
\IEEEauthorblockA{\IEEEauthorrefmark{2}Institut Supérieur de l'Aéronautique et de l'Espace (ISAE-SUPAERO), Université de Toulouse, 31055 Toulouse, FRANCE\\
Email: nicolas.drougard@isae-supaero.fr}
\IEEEauthorblockA{\IEEEauthorrefmark{3}ONERA - The French Aerospace Lab - Centre de Salon de Provence
Base Aérienne 701 - 13661 Salon Air, FRANCE\\
Email: bertille.somon@onera.fr}
}


\IEEEspecialpapernotice{(Final report)}

% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
\begin{abstract}
Trust is a crucial factor for the interaction of humans with automated systems. Brain Computer Interfaces allow to have direct communication pathways between a human’s cerebral activity and an external device. Passive BCIs are therefore suited to monitor an operator’s state of trust towards an automated system. Non-invasive BCI’s are usually implemented with EEG. This study investigated the state-of-the-art of human-machine trust detection using EEG. Literature review showed that the field had no generally accepted signal features associated to trust. This study analyzed Event Related Potentials stemming from an experiment investigating interpersonal trust by statistically testing amplitude variation on the P300 and FRN components. The present study did not find statistically significant differences in the signal for trust and distrust conditions.
\end{abstract}

\IEEEpeerreviewmaketitle

\section{Context}
\label{sec:problem-statement}
Automated systems are increasingly becoming indispensable to the lives of humans as their forms and usages proliferate in all fields from science, engineering, or medical treatments to trivial tasks of daily life. Whether people are familiar with the automated systems they interact with, directly or indirectly, even whether they actually wish to use them, is increasingly irrelevant because the massive implementations of these systems have made them inevitable in many cases. Autopilots, self-driving vehicles, automated fraud detection systems, translators, robots, plant monitoring systems, are just some examples of automated systems that already that have been vested in fields where they have or are ought to become essential \cite{lyons_transparency_2014}.

A crucial parameter to consider in the continuous spread of automated systems is however the trust human agents put in those systems. Trust is the very cement of most human interaction and a marker of any efficient organization. People know what they can expect from others because their experiences and their beliefs lead them to trust others will act in a certain way. However, humans cannot rely on their live-long experience, socialization, body, and verbal language to assess how they can trust an automated system – even more as they often do not know how it has been designed and how it works. 

Studies on trust between human and machines have shown two issues arising from the interaction between a human agent and an automated system: over-reliance or conversely under-use and misuse. Past studies have mainly focused on subjective measures of trust, usually using questionnaires in which participants in an experiment indicated their level of trust. This, however, does not allow to have real time feedback on the user’s trust or might be truncated by information influencing credibility for example. To have an objective, real time measure of trust, one thing to look at is neural activity. Research on neural activity, using electroencephalograms, has developed since the end of the nineties. Most research is focused on trust between human agents, papers investigating trust in automated systems based on neural activity are much scarcer. Literature mostly agrees human-human trust and human-machine trust cannot be amalgamated \cite{doi:10.1080/14639220500337708}

Ultimately, however, the goal is to have Brain Computer Interfaces that are able to have feedback on the operator’s trust level, and research from the field of passive BCI’s can be used to make significant progress in the case of trust. The most advanced BCI’s are active BCI’s, that use neural signals for motor activity, like robotic arms. Trust falls into the much less advanced field of passive BCI’s, that aim to measure abstract neural activity. One hurdle is the scarcity of available datasets to for researchers to work with \cite{hinss_open_2021}. Researchers have little to no incentives to share their data openly while the cost of acquiring such data is high. Furthermore, available datasets are often not complete or well-structured enough for other researchers to use.


\section{Problem statement}
\label{sec:problem-statement}

This project aims to provide an overview on the state-of-the-art of the methods described in literature concerning human trust in automated systems using EEG data. Furthermore, the goal is to find data-sets to test methods and find a consolidated approach to analyze EEG data for the determination of trust. The first challenge is to be able to identify markers of trust. Current research, concerning passive BCI's, is investigating whether it is possible to find features in the neural signals which can be attributed to trust levels using statistical analysis. 

These features will eventually be used as classifiers in order ascertain the trust level of an operator. Henceforward, these classifiers can be tested and found using machine learning techniques to automatize the detection of trust.

\section{Literature Review}
The first steps of this research project was to gather literature and possibly data-sets in order to assess the state-of-the-art in the field. The first finding is obtained promptly: literature on this subject is very scarce. Few scientific articles correspond to the keywords ‘EEG’ ‘automation’ and ‘trust’. Articles found under this keyword are of varying quality, even fewer were published in high profile scientific journals. Some have questionable results or nothing meaningful to takeaway. Although a beyond the focus of this project, the scope of research was extended to papers discussing the study of trust using EEG between humans, restricted however to experiments were humans interacted exclusively through a computer or were merely told they were interacting with another human meanwhile actually interacting with a program. Research has shown that such experiments were not equivalent to humans being told there were interacting with machines \cite{SOMON2019266}. Nevertheless, this scope extension allowed to take more articles into account and increased chances to find a suitable data-set significantly. 

Indeed, much neuro-scientific research, and therefore EEG data-sets, regarding human trust is emanates from the fields of psychology and is also often motivated by micro-economic research. However, our main purpose is to find suitable methods to exploit EEG data related to human-machine trust. Most studies relating to trust use self-reports as metrics, but the reliability of self-reports are questionable because of the bias participants might have towards an automated system, may it be positive or negative. Furthermore, self-reports do not allow to assess trust in real-time, which ultimately is the goal in order to have a BCI which is able to detect its users confidence. Studies investigating using human-machine trust by using EEG data are merely a dozen. To find a data-set to work with, a study regarding human-human trust with EEG data was used. While not ideal, findings and data from the field are usable for our goal to find methods to study trust with this type of data. 

Experiments conducted to study trust-related EEG data are mostly of the nature of some form of prisoner’s dilemma, where the propensity of an agent to cooperate reliably or not is used to assess trust components in a participant’s brain. This kind of experiment is mostly used in micro-economic or psychology related research. More specific to trust in automation is the flanker task experiment, it is the most established trust in automation experiment, as it is simple to carry out and to assess. Newer methods, developed for automated driving, are tasks were a participant can choose to rely or not on automatic piloting system. Trust is assessed with multiple frameworks and definition. there is currently no common definition for trust in the literature, nor are they common methods to analyze data-sets related to trust experiments \cite{elkins_comparison_nodate}.

Despite arguably not the best way to study trust, those experiments generate data which must be analyzed by using Event Related Potentials (or ERP) \cite{akash_classification_2018}. The principle of behind Event Related Potentials is to repeat a certain situation, over and over again, until noise from other cerebral activity can be eliminated by averaging the signal measured within a specific time window. These time windows are usually about 1.5 seconds long and represent an event. It is then possible to then find components in the signal which are related to a specific cerebral activity. In the case of motor cerebral activity this method performs very well for example, and the features found in the signal can be used to steer active BCI's. 

Event Related Potentials generated to analyze an abstract concept such as trust, versus a very concrete task of raising an arm for example, do not yield such results yet. In the literature, the most looked at components are the P300 and Error Related Negativity (ERN or Ne). The P300 event related potential is elicited in a human's decision making process as a reaction to an information. The response is a positive peak between 250ms and usually 350ms (this may vary) after a stimulus. In the frame of trust analysis, the P300 amplitude is thought to vary (positively) in the presence of trust \cite{blais_alpha_2019}.

In a similar way, the Error Related Negativity is an electric potential which is found when a human observes an error, committed by another human or arguably by an automated system. The ERN is usually observed between 80ms and 150ms after a stimulus. Again, it is not a potential which is specific to trust, but the varying amplitude of the potential may be related to trust and thous function as correlate or a feature of trust. 

Besides these commonly observed components, of which merely the amplitudes are associated to trust, the findings in current research are frayed and hard to unravel in order to consolidate any features of trust between studies. Nevertheless, it appears differences in the mean amplitudes on the P4, C4, C3 and Fz electrodes seem to recur across several studies as being related to trust. Generally, the central regions seem to be related to trust, as in deciding whether to trust \cite{akash_classification_2018}.

The drawback of ERPs is that they are time-locked to a very specific event which take place in a controlled laboratory environment. In more realistic, less controlled environments, the features extracted this way not prove to be robust enough to detect trust. The validity of such markers for uses in the real world is questionable. 

Research also investigates non-time-locked analysis by looking into continuous data. The signal is then transformed into the frequency domain with a Fourier transformation. EEG power spectra are usually decomposed alpha (8-13 Hz), beta (13-35 Hz), and gamma (higher than 35 Hz) bands. The evolution of the activity on these bands can then be analyzed to find correlates of trust, even independently of events. However, because the signal would contain lots of artifacts because of various stimulus and conditions, it might however be too noisy to find any features. However, analysis in the frequency domain is usually practiced with identical events and conditions generated in a controlled experimental environment. 

Similarly to findings in the time-domain however, there a little common findings between the few studies on trust correlates. It appears the energy of the beta band on the P3, P4, C3 and C4 electrodes might have variations that can be associated to trust. One study found suppression on the alpha band to be associated with the decision to trust or not to trust. Features found in current research are mostly sparse and difficult to generalize. In which manner results from very controlled experimental environments can be transposed to real world situations is a crucial topic in literature as research is looking for protocols which allow to be closer to real conditions: "As EEG experiments move to more naturalistic settings, neither time-locked analysis nor expert identification of stereotypical patterns may be possible."\cite{SU2018359}


\section{Data and Methods}

The data-set used for our analysis stems from "Trust Game Database: Behavioral and EEG Data From Two Trust Games" published in 2019 by Fu, Chao \& Yao, Xiaoqiang \& Yang, Xue \& Zheng, Lei \& Li, Jianbiao \& Wang, Yiwen \cite{fu_trust_2019}. The experiment aimed at providing data which allowed to investigate what the authors consider the most base issue in the field of trust: the factors that motivate people whether or not trust an unknown person. Specifically, the researchers wanted to provide a data-set to contribute to a open database for the study of the neural mechanisms of trust. They also deplored the scarcity of such resources while pointing out that the cost of generating those data-sets constitutes an hurdle for current research in the field. 

The experiment the data was collected from a modified version of Berg et al. ’s (1995) trust game identical to Wang et al., 2016 protocol. The participants played the role of the trustor while the trustee was actually a computer program (but the participants were told they interacted with another persons records. The trustor is given an endowment of 10 points. He can then choose between two options: to keep the ten points or to send them to the trustee, in which case the amount of points is tripled and the trustee receives 30 points. The trustee then makes a decision whether to split the his total 40 points (the trustee also had an initial endowment of 10 points) with the trustor, in which case each one's payoff is 20 points, or to keep it all for himself, in which case the trustee's payoff is 40 points and the trustor's, i.e. the participant's payoff is 0. Given the possibility of being exploited by the trustee, the trustor's decision to send money reflects his/her willingness to be vulnerable to the trustee's allocation decision, which is the behavioral operationalization of trust.

\begin{figure}[htp!]
    \centering
    \includegraphics[width=\linewidth]{experiment.png}
    \caption{Modified Berg experiment used for the data-set}
    \label{fig:my_label}
\end{figure}

For this experiment, the authors set up both a protocol of an iterated trust game and a one shot trust game with each 20 subjects. In the one shot protocol, participants were told that their counterparts were randomly selected people from a large pool of participants (N = 400) who were told to make a decision whether to send back 20 points or to keep 40 points one round only, and then playing with someone else. In the iterated trust game, the participant were told that they would be playing with the same person over multiple rounds. The decisions to reciprocate or not were actually made randomly across the rounds by the computer program with a cooperation rate of approximately 50\%.

The participants completed 150 rounds of the OTG or the ITG while their brain potentials were recorded using EEG. The following is the authors description of the protocol. The participant first saw a picture of a simplified decision tree showing possible outcomes for his/her single decision for 1,500 ms. After a variable 800~1,000 ms fixation cross, a picture indicating decision options was displayed in the center of the screen for 2,000 ms. During this time, the participant choose either to keep (cued by the number “10”) or to send (cued by the number “30”) his/her initial endowment by using his/her index finger to press either the “1” or “3” key on the keyboard, respectively. The position of decision options (10 and 30) as well as their mappings to the keys were counterbalanced between participants. If the participant failed to respond within 2,000 ms, a warning message that indicating he/she responds too slowly would be displayed to the participant and the round will be restarted. Following a variable 800~1,200 ms inter-stimuli interval with a black screen, the outcome of the participant's current trial and his/her current total scores were displayed for 1,200 and 2,000 ms, respectively. To make participants treat game points seriously, they were told that their participation compensation would be tied to the total number of game points earned in the game. 

The authors used a modified 10–20 system electrode cap (Neuroscan Inc.) with 64 channels. They furthermore describe that all EEG data were recorded using a 0.05–100 Hz bandpass filter and continuously sampled at 1000 Hz with the right mastoid reference and a forehead ground. The vertical electrooculography (EOG) activity was recorded with electrodes placed above and below the left eye, and the horizontal EOG was recorded from two electrodes placed 1.5 cm lateral to the left and right external canthi.

\subsection{Pre-processing}

We choose to the pipeline described in the paper to check if the results would converge with the ones displayed. However, we choose to work with the open-source Python package MNE, designed to work with human physiological data instead of MathWorks' MATLAB-EEG. The '.cnt' data was transformed to a MNE "raw" data object. To fit the data onto a standard 10-20 montage, channels 'CB1' and 'CB2', present in the data had to be removed. The data was then filtered with a high-pass filter at 1 Hz. A notch filter (instead of the band-stop filter used by the authors of the data-set) at 50 Hz width of 7 to remove electrical power noise. The filtered data was subsequently re-referenced to first and second mastoids. Bad channels were marked manually by visual analysis and when present and the data interpolated thereafter. 

To remove artifacts, ocular artifacts in particular, we used an Independent Component Analysis (ICA) provided in MNE, instead of the regression-based eye-movement correction algorithm implemented in SCAN software the authors used. 


\subsection{Post-processing}
Epochs were then extracted from the data 1000 ms before to 2000ms after each event, that is a feedback from the interface. We then performed a baseline correction ranging from -200ms to 0ms. 

\subsubsection{Time-domain Evoked objects}
The obtained Epochs were subsequently averaged for each type of event, that is the feedback after keeping the 10 points, i.e. 10 points, the feedback after being "betrayed" by the trustee, resulting in payoff of 0, and the feedback in the case of the trustee cooperates, which results in a payoff of 20 points. These averaged epochs for a same event for the same subject are called evoked and constitute our Event Related Potentials.

\subsubsection{Time-Frequency Evoked objects}
To extract time-frequency data from the epochs, the data was re-sampled to 500 Hz. We then used Morlet Wavelets for the Time-Frequency decomposition, the continuous estimate of time-frequency power in a given frequency band (3-35 Hz) as a function of time between -1,000ms and 2,000 ms was thous obtained. We performed a baseline correction between -400ms and -200ms and cropped the resulting image to a time window ranging from -200ms to 600ms. To obtain the images shown in the paper, we then averaged all epochs across for participants (evoked object) and then averaged for all participants. 

\section{First results and future work}
\subsection{Open Source Data-sets}

As discussed, it is difficult to find existing data-sets for EEG based experiments on trust analysis. Most studies investigate interpersonal correlates of trust, studies relating to human-machine trust are almost not to be found and there are no data-sets available without specific request. This means there is no actual open source data available at the moment, which hinders progress in the field. This is even more deplorable since most studies only use static techniques to analyze their data. Only 17\% of studies used machine learning techniques on their data-sets \cite{elkins_comparison_nodate}. Were there more data-sets, with complete labels and markers, for the scientific community to work with, much progress could be expected if researchers could try new ideas and different techniques then the authors originally intended. The fact is that there currently is no major data-set which would allow to find robust features or feature extracting methods. Existing studies all have their own findings, and while they do to some extent corroborate each other, there are no generalizable features to classify trust. 

\subsection{Statistical Analysis}
To analyze the quality of likely features of trust in the data-set we looked into the time windows related to the components frequently referred to in the literature. Precisely we looked into the time-windows corresponding to the P300 and ERN. We averaged the amplitude on their respective time windows for the trust and distrust conditions for each participant. We then performed a pairwise t-test to check whether significant differences in amplitude could be verified for each condition. 

An event characterized as "trust" means that the subject saw the feedback that his payoff is 20, which means his counterpart did cooperate, therefore he was correct to trust his counterpart. An event characterized as "betrayal" means the subject's payoff was 0, as his counterpart did not cooperate and sent nothing back. The decision to trust the counterpart was a wrong one, as the trust was betrayed.

\begin{figure}[htp!]
    \centering
    \includegraphics[width=\linewidth]{results_P300.png}
    \caption{Average amplitude between 250-350 ms after feedback event}
    \label{fig:my_label}
\end{figure}


Visually, we observe that the P300 seems to have lower amplitude when the participant's trust was betrayed versus when his trust was rewarded with cooperation. However, it is also noticeable that the outliers are numerous and widely scattered.

\begin{table}[htp!]
  \renewcommand{\arraystretch}{1.3}
  \caption{T-test on the average amplitude between 250-350ms or P300 for trust and betrayal condition}
  \label{tab:my-table}
  \centering
  \begin{tabular}{|c|c|c|c|c|}
    \hline
    Channel & T-critical & T-statistic & P-value & $H_0$\\
    \hline
    Oz & 1.761& $0.229$ & $0.822$ & not rejected\\
    \hline
    Pz & 1.761& $0.708$ & $0.490$ & not rejected\\
    \hline
    Cz & 1.761& $0.065$ & $0.948$ & not rejected\\
    \hline
    Fz & 1.761& $0.236$ & $0.816$ & not rejected\\
    \hline
  \end{tabular}
\end{table}

The observation that the P300 has stronger amplitude for the trust condition against the betrayal condition does not prove to be statistically significant. The critical t-value is not attained and the p-values are very high.

\begin{figure}[htp!]
    \centering
    \includegraphics[width=\linewidth]{results_ERN.png}
    \caption{Average amplitude between 80-150 ms after feedback event}
    \label{fig:my_label}
\end{figure}

The average amplitudes are more concentrated for the time-window related to the error related negativity (ERN). Again, the outliers are however quite scattered. Visually, we do not observe much difference between the trust and betrayal conditions on these channels.

\begin{table}[htp!]
  \renewcommand{\arraystretch}{1.3}
  \caption{T-test on the average amplitude between 80-150ms or ERN for trust and betrayal condition}
  \label{tab:my-table}
  \centering
  \begin{tabular}{|c|c|c|c|c|}
    \hline
    Channel & T-critical & T-statistic & P-value & $H_0$\\
    \hline
    Oz & -1.761& $-1.350$ & $0.198$ & not rejected  \\
    \hline
    Pz & -1.761& $-1.570$ & $0.138$ & not rejected\\
    \hline
    Cz & -1.761& $-1.429$ & $0.174$ & not rejected\\
    \hline
    Fz & -1.761& $-1.611$ & $0.129$ & not rejected\\
    \hline
  \end{tabular}
\end{table}

The results of the t-test again do not yield any significant difference in the amplitudes for the trust and betrayal conditions. However, we could notice that the t statistics much closer to the critical value and the p-values much lower (although still high) compared to the P300 time-window. 

These findings may have been flawed by the pre-processing, maybe bad channels were insufficiently detected, or maybe too much information was removed by the ICA.

\subsection{Feature Extraction}

The results on our data-set do not allow to extract robust features for usage by supervised learning algorithms. The features we have statistically tested were all intra-subject. While it would be possible to train algorithms on this data anyway, the risk is to over-fit to the data and to not be able to predict consistently whether a trust or distrust condition is fulfilled.

\section{Conclusion}
Our study found that in the current state of the field, very little data is available for researchers to explore. Most studies investigating trust using EEG-data relate to interpersonal trust. Experiments regarding trust human trust in automation using EEG are very few. It is also challenging to find complete data-sets with correct and easy understandable labels. The findings in the literature regarding features of trust in EEG data are sparse, although some studies do have intersecting results. Using an openly available data-set, we did not find statistically significant trust related features on the P300 and FRN intra-subject event related potentials evoked in the literature. It is questionable whether the type of experiment used for this data-set, and more generally in studies, are suitable for the detection of human trust in an automated system. In order to advance on trust detection for Brain Computer Interfaces, it appears necessary to design experiments where trust can be specifically targeted instead of inferred from a participants reaction to a specific event. Furthermore, research in this field requires data to be plentiful, complying to standards and freely available for progress to be made.

% Optional section
\section*{Acknowledgment}
I would like to express my deep gratitude to y project supervisors Bertille Somon and Nicolas Drougard, for their patient guidance, enthusiastic encouragement and useful critiques during this research work.

\bibliographystyle{plain}
\bibliography{reftrusteeg}

\nocite{*}

\end{document}
https://www.overleaf.com/project/6284a505b1b5f2811e1a97d2

