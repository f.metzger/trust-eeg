\begin{thebibliography}{10}

\bibitem{ajenaghughrure_psychophysiological_2021}
Ighoyota~Ben. Ajenaghughrure, Sonia~Claudia Da~Costa~Sousa, and David Lamas.
\newblock Psychophysiological modelling of trust in technology: Comparative
  analysis of algorithm ensemble methods.
\newblock In {\em 2021 {IEEE} 19th World Symposium on Applied Machine
  Intelligence and Informatics ({SAMI})}, pages 000161--000168. {IEEE}.

\bibitem{akash_classification_2018}
Kumar Akash, Wan-Lin Hu, Neera Jain, and Tahira Reid.
\newblock A classification model for sensing human trust in machines using
  {EEG} and {GSR}.
\newblock 8(4):1--20.

\bibitem{blais_alpha_2019}
Chris Blais, Derek~M. Ellis, Kimberly~M. Wingert, Adam~B. Cohen, and Gene~A.
  Brewer.
\newblock Alpha suppression over parietal electrode sites predicts decisions to
  trust.
\newblock 14(2):226--235.

\bibitem{choo_detecting_2022}
Sanghyun Choo and Chang Nam.
\newblock Detecting human trust calibration in automation: A convolutional
  neural network approach.
\newblock 52(4):774--783.

\bibitem{de_visser_learning_2018}
Ewart~J. de~Visser, Paul~J. Beatty, Justin~R. Estepp, Spencer Kohn, Abdulaziz
  Abubshait, John~R. Fedota, and Craig~G. {McDonald}.
\newblock Learning from the slips of others: Neural correlates of trust in
  automated agents.
\newblock 12:309.

\bibitem{dong_preliminary_2015}
Suh-Yeon Dong, Bo-Kyeong Kim, Kyeongho Lee, and Soo-Young Lee.
\newblock A preliminary study on human trust measurements by {EEG} for
  human-machine interactions.
\newblock In {\em Proceedings of the 3rd International Conference on
  Human-Agent Interaction}, pages 265--268. {ACM}.

\bibitem{elkins_comparison_nodate}
James~R Elkins.
\newblock Comparison of machine learning techniques on trust detection using
  {EEG}.
\newblock page 131.

\bibitem{fu_trust_2019}
Chao Fu, Xiaoqiang Yao, Xue Yang, Lei Zheng, Jianbiao Li, and Yiwen Wang.
\newblock Trust game database: Behavioral and {EEG} data from two trust games.
\newblock 10:2656.

\bibitem{hancock_meta-analysis_2011}
Peter~A. Hancock, Deborah~R. Billings, Kristin~E. Schaefer, Jessie Y.~C. Chen,
  Ewart~J. de~Visser, and Raja Parasuraman.
\newblock A meta-analysis of factors affecting trust in human-robot
  interaction.
\newblock 53(5):517--527.

\bibitem{hinss_open_2021}
Marcel~F. Hinss, Bertille Somon, Frederic Dehais, and Raphaelle~N. Roy.
\newblock Open {EEG} datasets for passive brain-computer interface
  applications: Lacks and perspectives.
\newblock In {\em 2021 10th International {IEEE}/{EMBS} Conference on Neural
  Engineering ({NER})}, pages 686--689. {IEEE}.

\bibitem{hu_real-time_2016}
Wan-Lin Hu, Kumar Akash, Neera Jain, and Tahira Reid.
\newblock Real-time sensing of trust in human-machine interactions**this
  material is based upon work supported by the national science foundation
  under award no. 1548616. any opinions, findings, and conclusions or
  recommendations expressed in this material are those of the author(s) and do
  not necessarily reflect the views of the national science foundation.
\newblock 49(32):48--53.

\bibitem{lawhern_eegnet_2018}
Vernon~J. Lawhern, Amelia~J. Solon, Nicholas~R. Waytowich, Stephen~M. Gordon,
  Chou~P. Hung, and Brent~J. Lance.
\newblock {EEGNet}: A compact convolutional network for {EEG}-based
  brain-computer interfaces.
\newblock 15(5):056013.

\bibitem{lotte_review_2018}
F~Lotte, L~Bougrain, A~Cichocki, M~Clerc, M~Congedo, A~Rakotomamonjy, and
  F~Yger.
\newblock A review of classification algorithms for {EEG}-based
  brain–computer interfaces: a 10 year update.
\newblock 15(3):031005.

\bibitem{lyons_transparency_2014}
Joseph~B. Lyons and Paul~R. Havig.
\newblock Transparency in a human-machine context: Approaches for fostering
  shared awareness/intent.
\newblock In Randall Shumaker and Stephanie Lackey, editors, {\em Virtual,
  Augmented and Mixed Reality. Designing and Developing Virtual and Augmented
  Environments}, pages 181--190. Springer International Publishing.

\bibitem{doi:10.1080/14639220500337708}
P.~Madhavan and D.~A. Wiegmann.
\newblock Similarities and differences between human–human and
  human–automation trust: an integrative review.
\newblock {\em Theoretical Issues in Ergonomics Science}, 8(4):277--301, 2007.

\bibitem{SU2018359}
Kyung min Su, W.~David Hairston, and Kay Robbins.
\newblock Eeg-annotate: Automated identification and labeling of events in
  continuous signals with applications to eeg.
\newblock {\em Journal of Neuroscience Methods}, 293:359--374, 2018.

\bibitem{oh_neurological_2020}
Seeung Oh, Younho Seong, Sun Yi, and Sangsung Park.
\newblock Neurological measurement of human trust in automation using
  electroencephalogram.
\newblock 20(4):261--271.

\bibitem{somon_correlats_nodate}
Bertille Somon.
\newblock Corrélats neuro-fonctionnels du phénomène de sortie de boucle:
  impacts sur le monitoring des performances.
\newblock page 180.

\bibitem{SOMON2019266}
Bertille Somon, Aurélie Campagne, Arnaud Delorme, and Bruno Berberian.
\newblock Human or not human? performance monitoring erps during human agent
  and machine supervision.
\newblock {\em NeuroImage}, 186:266--277, 2019.

\bibitem{wang_eeg-based_2018}
Min Wang, Aya Hussein, Raul~Fernandez Rojas, Kamran Shafi, and Hussein~A.
  Abbass.
\newblock {EEG}-based neural correlates of trust in human-autonomy interaction.
\newblock In {\em 2018 {IEEE} Symposium Series on Computational Intelligence
  ({SSCI})}, pages 350--357. {IEEE}.

\end{thebibliography}
